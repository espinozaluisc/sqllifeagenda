package com.example.sqllifeagenda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import database.AgendaContactos;
import database.AgendaDbHelper;
import database.Contacto;

public class MainActivity extends AppCompatActivity {


    private TableLayout tabla1;
    private TextView lblNombre;
    private EditText txtNombre;
    private TextView lblTel1;
    private TextView lblTel2;
    private EditText txtTel1;
    private EditText txtTel2;
    private TextView lblDir;
    private EditText txtDomicilio;
    private TextView lblNotas;
    private EditText txtNotas;
    private CheckBox chkFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnListar;
    private Button btnCerrar;
    private Contacto savedContact;
    //Private int savedIndex;
    private long id;
    private AgendaContactos db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        tabla1 = (TableLayout) findViewById(R.id.tabla1);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        lblTel1 = (TextView) findViewById(R.id.lblTel1);
        lblTel2 = (TextView) findViewById(R.id.lblTel2);
        txtTel1 = (EditText) findViewById(R.id.txtTel1);
        txtTel2 = (EditText) findViewById(R.id.txtTel2);
        lblDir = (TextView) findViewById(R.id.lblDir);
        txtDomicilio = (EditText) findViewById(R.id.txtDomicilio);
        lblNotas = (TextView) findViewById(R.id.lblNotas);
        txtNotas = (EditText) findViewById(R.id.txtNotas);
        chkFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        db = new AgendaContactos(MainActivity.this);


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDomicilio.getText().toString().matches("") || txtNombre.getText().toString().matches("")
                        || txtTel1.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    nContacto.setNombre(txtNombre.getText().toString());
                    nContacto.setTelefono1(txtTel1.getText().toString());
                    nContacto.setTelefono2(txtTel2.getText().toString());
                    nContacto.setDomicilio(txtDomicilio.getText().toString());
                    nContacto.setNotas(txtNotas.getText().toString());
                    nContacto.setFavorito(chkFavorito.isChecked() ? 1 : 0);
                    db.openDataBase();
                    if(savedContact==null){
                        long idx = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this,"Se agrego correctamente con ID: "+idx, Toast.LENGTH_SHORT).show();
                        limpiar();

                    }
                    else{
                        db.UpdateContacto(nContacto,id);
                        Toast.makeText(MainActivity.this,"Se Actualizo el registro: " +id, Toast.LENGTH_SHORT).show();

                    }
                    db.cerrar();
                }
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(i, 0);
            }
        });

    }

    public void limpiar() {
        txtNotas.setText("");
        txtNombre.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtDomicilio.setText("");
        chkFavorito.setChecked(false);
    }
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        if(Activity.RESULT_OK==resultCode){
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id=contacto.getID();
            txtNombre.setText(savedContact.getNombre());
            txtTel1.setText(savedContact.getTelefono1());
            txtTel2.setText(savedContact.getTelefono2());
            txtDomicilio.setText(savedContact.getDomicilio());
            txtNotas.setText(savedContact.getNotas());
            if(contacto.getFavorito()>0)
            chkFavorito.setChecked(true);
            else
                limpiar();
        }
    }
}
